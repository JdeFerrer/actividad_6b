dias=1
maxd=`cat precipitaciones.txt | wc -l`

while [ $dias -le $maxd ]; do
    if [ `cat precipitaciones.txt | awk '{print $2}' | head -$dias | tail -1` -le 0 ]; then
	dia=`cat precipitaciones.txt | awk '{print $1}'| head -$dias | tail -1`
	case $dia in
	    1)
		echo "El lunes día 1 no hubo precipitaciones"
	    ;;
	    2) 
		echo "El martes día 2 no hubo precipitaciones"
	    ;;
	    3)
		echo "El miércoles día 3 no hubo precipitaciones"
	    ;;
	    4)
		echo "El jueves día 4 no hubo precipitaciones"
	    ;;
	    5)
		echo "El viernes día 5 no hubo precipitaciones"
	    ;;
	    6)
		echo "El sábado día 6 no hubo precipitaciones"
	    ;;
	    7)
		echo "El domingo día 7 no hubo precipitaciones"
	    ;;
	    8)
		echo "El lunes día 8 no hubo precipitaciones"
	    ;;
	    9)
		echo "El martes día 9 no hubo precipitaciones"
	    ;;
	    10)
		echo "El miércoles día 10  no hubo precipitaciones"
	    ;;
	esac
    fi
    dias=$(($dias+1))
done